package Wrapper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import Common.Constant;
import Helper.Logger;

public class DriverManager {

	private static ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	static Logger logger = new Logger(DriverManager.class);

	public static void createDriver(String browser, String mode) {
		logger.info("Init driver of browser: " + browser);
		if (mode.equals("Local")) {
			if (browser.equals("Chrome")) {
				System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER_PATH);
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-infobars", "--incognito", "--start-maximized");
				driver.set(new ChromeDriver(options));
			} else {
				System.setProperty("webdriver.gecko.driver", Constant.FIREFOX_DRIVER_PATH);
				FirefoxOptions options = new FirefoxOptions();
				options.addArguments("-incognito", "-start-maximized");
				driver.set(new FirefoxDriver(options));
			}
		} else if (mode.equals("Remote")) {
			if (browser.equals("Chrome")) {
				System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER_PATH);
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-infobars", "--incognito", "--start-maximized");
				DesiredCapabilities desiredCapabilities = new DesiredCapabilities().chrome();
				desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
				try {
					driver.set(new RemoteWebDriver(new URL(Constant.NODE_URL), desiredCapabilities));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}

			} else if (mode.equals("Firefox")) {
				System.setProperty("webdriver.gecko.driver", Constant.FIREFOX_DRIVER_PATH);
				FirefoxOptions options = new FirefoxOptions();
				options.addArguments("-incognito", "-start-maximized");
				DesiredCapabilities desiredCapabilities = new DesiredCapabilities().firefox();
				desiredCapabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
				try {
					driver.set(new RemoteWebDriver(new URL(Constant.NODE_URL), desiredCapabilities));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static WebDriver getDriver() {
		return driver.get();
	}

	public static void navigateTo(String url) {
		logger.info("Navigate to: " + url);
		getDriver().navigate().to(url);
	}

	public static String getCurrentUrl() {
		logger.info("Get current url");
		return getDriver().getCurrentUrl();
	}

	public static String getTitle() {
		logger.info("Get current title");
		return getDriver().getTitle();
	}

	public static List<WebElement> findElements(By by) {
		logger.info("Find list elements by: " + by.toString());
		return getDriver().findElements(by);
	}

	public static WebElement findElement(By by) {
		logger.info("Find element by: " + by.toString());
		return getDriver().findElement(by);
	}

	public static void close() {
		logger.info("Close driver");
		getDriver().close();
	}

	public static void quit() {
		logger.info("Quit driver");
		getDriver().quit();
	}

	public static void maximize() {
		logger.info("Maximize window");
		getDriver().manage().window().maximize();
	}

	public static Set<String> getWindowHandles() {
		logger.info("Get windows");
		return getDriver().getWindowHandles();
	}

	public static String getWindowHandle() {
		logger.info("Get window");
		return getDriver().getWindowHandle();
	}

	public static void switchTo(int tabIndex) {
		logger.info("Switch to tab " + (tabIndex + 1));
		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs.get(tabIndex));
	}

	// Alert
	public static boolean doesMessageAlert(String msg, Boolean doesContain) {
		logger.info("Does alert message correct: " + msg);
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		long timeElapsed = stopwatch.getTime();
		while(timeElapsed <= Constant.WAIT_MID_TIME*1000) {
			try {
				String currentAlertMsg = getDriver().switchTo().alert().getText();
				if (doesContain) {
					stopwatch.stop();
					return currentAlertMsg.contains(msg);
				}
				stopwatch.stop();
				return currentAlertMsg.equals(msg);
			} catch (Exception error) {
				timeElapsed = stopwatch.getTime();
			}
		}
		stopwatch.stop();
		return false;
	}

	public static boolean doesMessageAlert(String msg) {
		return doesMessageAlert(msg, false);
	}

	public static void acceptAlert() {
		logger.info("Accept alert");
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		long timeElapsed = stopwatch.getTime();
		while(timeElapsed <= Constant.WAIT_MID_TIME*1000) {
			try {
				getDriver().switchTo().alert().accept();
				break;
			} catch (Exception error) {
				timeElapsed = stopwatch.getTime();
			}
		}
		stopwatch.stop();
	}

	public static void dismissAlert() {
		logger.info("Dismiss alert");
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		long timeElapsed = stopwatch.getTime();
		while(timeElapsed <= Constant.WAIT_MID_TIME*1000) {
			try {
				getDriver().switchTo().alert().dismiss();
				break;
			} catch (Exception error) {
				timeElapsed = stopwatch.getTime();
			}
		}
		stopwatch.stop();
	}
}
