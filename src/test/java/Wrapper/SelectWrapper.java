package Wrapper;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common.Constant;
import Helper.Logger;

public class SelectWrapper extends WebElementWrapper {
	static Logger logger = new Logger(SelectWrapper.class);

	public SelectWrapper(By locator) {
		super(locator);
	}

	public Select waitForSelectControlVisible(int waitingTime, boolean doesThrowException) {
		logger.info("Wait for element visible in: " + waitingTime + " second");
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), waitingTime);
		int count = 0;
		while (count != Constant.WAIT_SHORT_TIME) {
			try {
				WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(this.locator));
				return new Select(element);
			} catch (StaleElementReferenceException error) {
				// TODO: handle exception
				count++;
			} catch (Exception error) {
				if (doesThrowException) {
					logger.warn("Throw Error: " + error);
					throw error;
				}
				break;
			}
		}
		return null;
	}

	public Select waitForSelectControlVisible(int waitingTime) {
		return waitForSelectControlVisible(waitingTime, false);
	}
	
	public Select waitForSelectControlVisible() {
		return waitForSelectControlVisible(Constant.WAIT_MID_TIME);
	}

	public void selectByText(String text) {
		logger.info("Select option with text: " + text);
		waitForSelectControlVisible().selectByVisibleText(text);
	}

	public String getFirstSelectedOption() {
		logger.info("Get first option is selected");
		return waitForSelectControlVisible().getFirstSelectedOption().getText();
	}

	public List<String> getSelectOptions() {
		logger.info("Get list options of select control by locator: " + locator);
		List<String> listOption = new ArrayList<String>();
		List<WebElement> options = waitForSelectControlVisible().getOptions();
		for (WebElement item : options) {
			listOption.add(item.getText());
		}
		return listOption;
	}

	public boolean doesSelectOptionExist(String option) {
		logger.info("Does " + option + " option exist in select control by locator: " + locator);
		return getSelectOptions().contains(option);
	}

	public boolean doesSelectOptionsExist(List<String> options) {
		logger.info("Does list options: " + options + " exist in select control by locator: " + locator);
		List<String> currentOptionList = getSelectOptions();
		if (currentOptionList.size() < options.size()) {
			return false;
		}
		int optionsSize = options.size();
		for (int i = 0; i < optionsSize + 1; i++) {
			if (!doesSelectOptionExist(options.get(0))) {
				return false;
			}
		}
		return true;
	}
}
