package Test.RegisterTest;

import java.awt.AWTException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.testng.annotations.Test;

import DataObjects.RegisterInfo;
import DataObjects.RoleOptions;
import Helper.Logger;
import Helper.Ultilities;
import PageObject.Pages.BusinessEditPage;
import PageObject.Pages.BusinessViewPage;
import PageObject.Pages.CompleteRegisterPage;
import PageObject.Pages.IdentifyOnfidoPage;
import PageObject.Pages.IdentifyViewPage;
import PageObject.Pages.IncorporateSelectorPage;
import PageObject.Pages.LoginPage;
import PageObject.Pages.PersonEditPage;
import PageObject.Pages.PersonViewPage;
import PageObject.Pages.RegisterPage;
import PageObject.Pages.RegisterSelectMethodPage;
import PageObject.Pages.VerifyEmailOtpPage;
import PageObject.Pages.VerifyPhoneOtpPage;

public class RegisterTest extends BaseTest {
	Logger logger = new Logger(RegisterTest.class);

	@Test(testName = "QA-test", description = "pretest QA")
	public void TestCase_001() throws AWTException {
		//prepare data
		String fullname = Ultilities.generateRandomString();
		String email = Ultilities.generateRandomGmail();
		String phoneNumber = Ultilities.generateRandomStringNumber();
		String role = RoleOptions.APPOINTED_DIRECTOR.getValue();
		String selectHearingOption = "Others";
		String promo = Ultilities.generateRandomStringNumber();
		boolean agreeTerm = true;
		String currentMonth = "January";
		String monthOfBirth = "August";
		String currentYear = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));;
		String yearOfBirth = "2000";
		String dayOfBirth = "19";
		String gender = "Male";
		String country = "Viet Nam";
		String industry = "Retail Services";
		String subIndustry = "Automotive & Cars";
		String registrationType = "Sole proprietorship";
		List<String> expectedInterestedProducList = new ArrayList<String>(
				Arrays.asList("Credit Line", "Debit Account", "Others"));
		RegisterInfo registerInfo = new RegisterInfo(fullname, email, phoneNumber, role, selectHearingOption, promo, agreeTerm);
		
		logger.reportStep("Navigate to default url");
		LoginPage loginPage = new LoginPage();
		
		logger.reportStep("navigate to register page");
		RegisterPage registerPage = loginPage.goToRegisterPage();
		
		logger.reportStep("Input all required data");
		registerPage.inputAllInfo(registerInfo);
		
//		logger.reportStep("Navigate to Verify Phone OTP page");
//		VerifyPhoneOtpPage verifyPhoneOTPPage = registerPage.gotoVerifyPhoneOtpPage();
//		
//		logger.reportStep("Input default OTP : 1234");
//		CompleteRegisterPage completeRegisterPage = verifyPhoneOTPPage.inputDefaultOTP();
//		
//		logger.reportStep("Navigate to Incorporate Selector Page");
//		IncorporateSelectorPage incorporateSelectorPage = completeRegisterPage.gotoIncorporateSelectorPage();
//		
//		logger.reportStep("Navigate to Register Select Method Page");
//		RegisterSelectMethodPage registerSelectMethodPage = incorporateSelectorPage.gotoRegisterSelectMethodPage();
//		
//		logger.reportStep("Navigate to Person View Page");
//		PersonViewPage personViewPage = registerSelectMethodPage.gotoPersonViewPage();
//		
//		logger.reportStep("Navigate to Person Edit Page");
//		PersonEditPage personEditPage = personViewPage.gotoPersonEditPage();
//		
//		logger.reportStep("Select day of birth");
//		personEditPage.openInputDayOfBirth();
//		personEditPage.selectYearOfBirth(yearOfBirth, currentYear);
//		personEditPage.selectMonthOfBirth(monthOfBirth, currentMonth);
//		personEditPage.selectDayOfBirth(dayOfBirth);
//		
//		logger.reportStep("Select gender");
//		personEditPage.selectGender(gender);
//		
//		logger.reportStep("Select Interested Product");
//		personEditPage.selectInterestedProduct(expectedInterestedProducList);
//		
//		logger.reportStep("Select nationality");
//		personEditPage.selectNationality(country);
//		
//		logger.reportStep("Navigate to Verify Email Otp Page");
//		VerifyEmailOtpPage verifyEmailOtpPage = personEditPage.gotoVerifyEmailOtpPage();
//		
//		logger.reportStep("Input default OTP : 1234");
//		BusinessViewPage businessViewPage = verifyEmailOtpPage.inputDefaultOTP();
//		
//		logger.reportStep("Navigate to Business Edit Page");
//		BusinessEditPage businessEditPage = businessViewPage.gotoBusinessEditPage();
//		
//		logger.reportStep("Input Bussiness Name");
//		businessEditPage.enterBussinessName(fullname);
//		
//		logger.reportStep("Select Registration Type");
//		businessEditPage.selectRegistrationType(registrationType);
//		
//		logger.reportStep("Select Registration Business Industry");
//		businessEditPage.selectRegisterBusinessIndustry(industry);
//		
//		logger.reportStep("Select Registration Business SubIndustry");
//		businessEditPage.selectRegisterBusinessSubIndustry(subIndustry);
//		
//		logger.reportStep("Input random UEN number");
//		businessEditPage.inputUENNumber();
//		
//		logger.reportStep("Navigate to Identify View Page");
//		IdentifyViewPage identifyViewPage = businessEditPage.gotoIdentifyViewPage();
//		
//		logger.reportStep("Navigate to Identify Onfido Page");
//		IdentifyOnfidoPage inIdentifyOnfidoPage = identifyViewPage.gotoIdentifyOnfidoPage();
//		
//		logger.reportStep("Navigate to check verify");
//		inIdentifyOnfidoPage.gotoVerify();
//		
//		logger.reportStep("VP. Verify that Dashboard Mainpage appears");
	}
}
