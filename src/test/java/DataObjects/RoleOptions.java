package DataObjects;


public enum RoleOptions {
	APPOINTED_DIRECTOR("Appointed director"), 
	NON_DIRECTOR("Non-director");

	private String value;

	RoleOptions(String value) {
			this.value = value;
		}

	public String getValue() {
		return this.value;
	}
}
