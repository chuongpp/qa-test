package Common;

import Helper.Properties;
import Helper.Ultilities;

public class Constant {

	public static final String PROPERTIES_PATH = Ultilities.getProjectPath()
			+ "/src/test/resources/config/config.properties";
	public static final String LOG4J_PATH = Ultilities.getProjectPath() + "/src/test/resources/config/log4j.xml";
	public static final String LOG4J_LOG_FILE = Ultilities.getProjectPath() + "/src/test/resources/logs/log.txt";

	public static final String CHROME_DRIVER_PATH = Ultilities.getProjectPath()
			+ "/src/test/resources/driver/chromedriver.exe";
	public static final String FIREFOX_DRIVER_PATH = Ultilities.getProjectPath()
			+ "/src/test/resources/driver/geckodriver.exe";

	public static final String URL = Properties.getValueOf("url");
	
	public static final String NODE_URL = Properties.getValueOf("nodeURL");

	public static final int WAIT_SHORT_TIME = Integer.parseInt(Properties.getValueOf("waitShortTime"));
	public static final int WAIT_MID_TIME = Integer.parseInt(Properties.getValueOf("waitMidTime"));
	public static final int WAIT_LONG_TIME = Integer.parseInt(Properties.getValueOf("waitLongTime"));
	public static final int SAMPLE_COUNT_TIME = Integer.parseInt(Properties.getValueOf("sampleCountTime"));

	public static final String SAMPLE_FORMAT_DATE_TIME = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMAT_DATE_TIME_TO_REPORT = "dd-MM-yyyy HH.mm.ss";
	
}
