package Helper;

import java.text.SimpleDateFormat;
import java.util.Date;


public class DateHelper {
	static Date date;
	
	public DateHelper() {
		date = new Date();
	}
	
	public DateHelper(Date dateModified) {
		date = dateModified;
	}
    
    public String formatDateTime(String formatDateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat(formatDateTime);  
        return formatter.format(date);
    }
}
