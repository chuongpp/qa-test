package Helper;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnhandledAlertException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import Wrapper.DriverManager;

public class Reporter {
	static ExtentReports extentReports;
	private static ThreadLocal<ExtentTest> extentTest = new ThreadLocal<ExtentTest>();
	private static ThreadLocal<ExtentTest> extentNode = new ThreadLocal<ExtentTest>();
	static String resultsFolderPath;

	public static void createReporter(String resultsFolder) {
		resultsFolderPath = resultsFolder;
		ExtentSparkReporter extentSparkReporter = new ExtentSparkReporter(resultsFolderPath + "\\Results.html");
		extentReports = new ExtentReports();
		extentReports.attachReporter(extentSparkReporter);
	}
	
	public static ExtentTest getExtentTest() {
		return extentTest.get();
	}
	
	public static ExtentTest getExtentNode() {
		return extentNode.get();
	}
	
	public static void createTest(String name) {
		extentTest.set(extentReports.createTest(name));
	}

	public static void createNode(String step) {
		extentNode.set(getExtentTest().createNode(step));
	}

	public static void logInfo(String info) {
		getExtentTest().info(info);
	}

	public static void logPass(String passString) {
		getExtentTest().pass(passString);
	}

	public static void logSkip(String skipString) {
		getExtentTest().skip(skipString);
	}

	public static void logFail(String failString) {
		getExtentTest().fail(failString);
	}

	public static void flush() {
		extentReports.flush();
	}

	public static void captureScreen(String fileName) {
		TakesScreenshot screen = (TakesScreenshot) DriverManager.getDriver();
		fileName = Ultilities.removeSpecialCharacters(fileName);
		File src = null;
		try {
			src = screen.getScreenshotAs(OutputType.FILE);
		} catch (UnhandledAlertException alertException) {
			src = screen.getScreenshotAs(OutputType.FILE);
		}
		String dest = resultsFolderPath + "//" + fileName + ".png";
		File target = new File(dest);
		try {
			FileUtils.copyFile(src, target);
		} catch (IOException e) {
			e.printStackTrace();
		}
		getExtentNode().fail("Screenshot", MediaEntityBuilder.createScreenCaptureFromPath(dest).build());
	}
}
