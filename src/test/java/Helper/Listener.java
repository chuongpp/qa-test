package Helper;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.google.common.base.Throwables;

public class Listener implements ITestListener {
	static Logger logger = new Logger(Listener.class);

	public void onFinish(ITestContext context) {
		logger.info("--------End Suite " + context.getName() + "--------");
		Reporter.flush();
	}

	public void onTestSuccess(ITestResult result) {
		logger.info(result.getName() + ": Passed");
		Reporter.logPass("All Passed");
	}

	public void onTestFailure(ITestResult result) {
		logger.error(result.getName() + ": Failed");
		String errorMsg = Throwables.getStackTraceAsString(result.getThrowable());
		logger.error(errorMsg);
		Reporter.logFail(errorMsg);
		Reporter.captureScreen(result.getName());
	}

	public void onTestSkipped(ITestResult result) {
		logger.info(result.getName() + ": Skipped");
		Reporter.logInfo("Skipped");
		String errorMsg = Throwables.getStackTraceAsString(result.getThrowable());
		logger.error(errorMsg);
		Reporter.logSkip(errorMsg);
		Reporter.captureScreen(result.getName());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		logger.error(result.getName() + ": Failed");
		String errorMsg = Throwables.getStackTraceAsString(result.getThrowable());
		logger.error(errorMsg);
		Reporter.logFail(errorMsg);
		Reporter.captureScreen(result.getName());
	}

}
