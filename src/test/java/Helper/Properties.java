package Helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import Common.Constant;

public class Properties {
    static String result = "";
	static InputStream inputStream;
	static java.util.Properties prop = new java.util.Properties();
	
	public static String getValueOf(String item){
		 
		try {
			inputStream = new FileInputStream(Constant.PROPERTIES_PATH);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file not found");
			}
 
			// get the property value
			result = prop.getProperty(item);
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
}
