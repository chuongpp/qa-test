package PageObject.Pages;

import Wrapper.WebElementWrapper;

public class IncorporateSelectorPage extends GeneralPage{
	WebElementWrapper btnMyBusinessIsRegistered = new WebElementWrapper("//div[contains(@class,'incorporated_selector__separator')]//button");
	
	public IncorporateSelectorPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnMyBusinessIsRegistered.waitForElementVisible();
		}
	}

	public IncorporateSelectorPage() {
		btnMyBusinessIsRegistered.waitForElementVisible();
	}
	
	public RegisterSelectMethodPage gotoRegisterSelectMethodPage() {
		btnMyBusinessIsRegistered.click();
		return new RegisterSelectMethodPage();
	}
}
