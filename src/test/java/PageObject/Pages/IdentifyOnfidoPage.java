package PageObject.Pages;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class IdentifyOnfidoPage extends GeneralPage{
	WebElementWrapper btnBeginVerification = new WebElementWrapper("//button[@type='submit']");
	WebElementWrapper bodyVerifyYourIdentity = new WebElementWrapper("//div[@class='onfido-sdk-ui-Select-wrapper']");

	public IdentifyOnfidoPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnBeginVerification.waitForElementVisible();
		}
	}

	public IdentifyOnfidoPage() {
		btnBeginVerification.waitForElementVisible(Constant.WAIT_LONG_TIME);
	}

	public void gotoVerify() {
		btnBeginVerification.click();
		bodyVerifyYourIdentity.waitForElementVisible();
	}
}
