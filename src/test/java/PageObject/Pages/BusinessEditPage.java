package PageObject.Pages;

import Helper.Ultilities;
import Wrapper.WebElementWrapper;

public class BusinessEditPage extends GeneralPage{
	WebElementWrapper btnSubmit = new WebElementWrapper("//button[@type='submit']");
	WebElementWrapper txtBussinessName = new WebElementWrapper("//input[@autocomplete='organization']");
	WebElementWrapper txtRegistrationNumber = new WebElementWrapper("//input[@placeholder='Business Registration Number UEN']");
	WebElementWrapper btnDropBoxRegistrationType = new WebElementWrapper("//div[@data-cy='register-business-registration-type']/../following-sibling::div/i");
	WebElementWrapper btnDropBoxRegisterBusinessIndustry = new WebElementWrapper("//div[@data-cy='register-business-industry']/../following-sibling::div/i");
	WebElementWrapper btnDropboxRegisterBusinessSubIndustry = new WebElementWrapper("//div[@data-cy='register-business-sub-industry']/../following-sibling::div/i");
	WebElementWrapper optionDropbox = new WebElementWrapper("//div[@class='q-menu q-position-engine scroll q-menu--square']//div[@class='q-item__label' and text()='{0}']");
	
	public BusinessEditPage(Boolean isDisplayed) {
		if (isDisplayed) {
			txtBussinessName.waitForElementVisible();
		}
	}

	public BusinessEditPage() {
		txtBussinessName.waitForElementClickable();
	}
	
	public void enterBussinessName(String bussinessName) {
		txtBussinessName.enter(bussinessName);
	}
	
	public void selectRegistrationType(String bussinessName) {
		btnDropBoxRegistrationType.click();
		while (true) {
        	if (optionDropbox.getElementWithParameters(bussinessName).doesExist()) {
        		optionDropbox.getElementWithParameters(bussinessName).click();
        		break;
			}
        	else {
        		btnDropBoxRegistrationType.click();
			}		
		}
	}
	
	public void selectRegisterBusinessIndustry(String industry) {
		btnDropBoxRegisterBusinessIndustry.click();
		while (true) {
        	if (optionDropbox.getElementWithParameters(industry).doesExist()) {
        		optionDropbox.getElementWithParameters(industry).click();
        		break;
			}
        	else {
        		btnDropBoxRegisterBusinessIndustry.click();
			}		
		}
	}
	
	public void selectRegisterBusinessSubIndustry(String subindustry) {
		btnDropboxRegisterBusinessSubIndustry.click();
		while (true) {
        	if (optionDropbox.getElementWithParameters(subindustry).doesExist()) {
        		optionDropbox.getElementWithParameters(subindustry).click();
        		break;
			}
        	else {
        		btnDropboxRegisterBusinessSubIndustry.click();
			}		
		}
	}
	
	public void inputUENNumber() {
		String uenNumuerString = Ultilities.generateRandomStringNumber(9)+ "a";
		txtRegistrationNumber.enter(uenNumuerString);
	}
	
	public IdentifyViewPage gotoIdentifyViewPage() {
		btnSubmit.click();
		return new IdentifyViewPage();
	}
}
