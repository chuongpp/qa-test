package PageObject.Pages;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class IdentifyViewPage extends GeneralPage {
	WebElementWrapper btnGetStart = new WebElementWrapper("//button[@type='submit']//span[text()='Get Started']");

	public IdentifyViewPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnGetStart.waitForElementVisible();
		}
	}

	public IdentifyViewPage() {
		btnGetStart.waitForElementVisible(Constant.SAMPLE_COUNT_TIME);
	}

	public IdentifyOnfidoPage gotoIdentifyOnfidoPage() {
		btnGetStart.click();
		return new IdentifyOnfidoPage();
	}
}
