package PageObject.Pages;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class LoginPage extends GeneralPage{
	WebElementWrapper hyperLinkRegister = new WebElementWrapper("//a[contains(@href,'register')]");
	
	public LoginPage(Boolean isDisplayed) {
		if (isDisplayed) {
			hyperLinkRegister.waitForElementVisible();
		}
	}

	public LoginPage() {
		hyperLinkRegister.waitForElementVisible(Constant.SAMPLE_COUNT_TIME);
	}

	public RegisterPage goToRegisterPage() {
		hyperLinkRegister.click();
		return new RegisterPage();
	}
}
