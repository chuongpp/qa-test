package PageObject.Pages;

import Wrapper.WebElementWrapper;

public class CompleteRegisterPage extends GeneralPage{
	WebElementWrapper btnContinue = new WebElementWrapper("//span[text() = 'Continue']");
	
	public CompleteRegisterPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnContinue.waitForElementVisible();
		}
	}

	public CompleteRegisterPage() {
		btnContinue.waitForElementVisible();
	}
	
	public IncorporateSelectorPage gotoIncorporateSelectorPage() {
		btnContinue.click();
		return new IncorporateSelectorPage();
	}
}
