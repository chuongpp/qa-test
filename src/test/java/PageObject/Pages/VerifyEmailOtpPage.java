package PageObject.Pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import Common.Constant;
import Wrapper.WebElementWrapper;

public class VerifyEmailOtpPage extends GeneralPage{
	WebElementWrapper btnResendOTP = new WebElementWrapper("//span[text() = 'Resend OTP']");

	public VerifyEmailOtpPage(Boolean isDisplayed) {
		if (isDisplayed) {
			btnResendOTP.waitForElementVisible();
		}
	}

	public VerifyEmailOtpPage() {
		btnResendOTP.waitForElementClickable(Constant.SAMPLE_COUNT_TIME);
	}
	
	public BusinessViewPage inputDefaultOTP() throws AWTException {
		Robot robot = new Robot();
		robot.delay(5000);
		robot.keyPress(KeyEvent.VK_1);
		robot.keyRelease(KeyEvent.VK_1);
		robot.keyPress(KeyEvent.VK_2);
		robot.keyRelease(KeyEvent.VK_2);
		robot.keyPress(KeyEvent.VK_3);
		robot.keyRelease(KeyEvent.VK_3);
		robot.keyPress(KeyEvent.VK_4);
		robot.keyRelease(KeyEvent.VK_4);
		return new BusinessViewPage();
	}
	
}
