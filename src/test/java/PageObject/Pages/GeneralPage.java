package PageObject.Pages;

import Wrapper.WebElementWrapper;

public class GeneralPage {
	WebElementWrapper header = new WebElementWrapper("//header");
	
	public GeneralPage(Boolean isDisplayed) {
		if (isDisplayed) {
			header.waitForElementVisible();
		}
	}

	public GeneralPage() {
		header.waitForElementVisible();
	}
}
