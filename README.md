==========================================================================================================
A. Setup framework
==========================================================================================================

	1. Install required software and libraries below:
		1.1 Eclipse
		1.2 Java JDK 1.8
		1.3 Maven
		
	2. Build steps
		2.1 Select pom.xml -> right click -> Run As -> Maven clean
		2.2 Select pom.xml -> right click -> Run As -> Maven install
	
	3. Execution via Eclipse
		3.1 Go to folder "src/test/resource/suites"
		3.2 Select any test suite file you want to run -> right click -> Run As -> TestNG suite


==========================================================================================================
B. Test configuration
==========================================================================================================

	1. Set parameter values in test suite to specific browserwill be run
	   Ex:
		<parameter name="browser" value="Chrome" />
		<parameter name="remote" value="Local" />
		<suite thread-count="1" parallel="methods" name="Suite">

		1.1 browser [browserName]   ----> main browser to run tests
			1.1.1 browserName   ----> Chrome/Firefox

		1.2 remote [remoteName]   ----> run test case on Local/Remote enviroment
		
		1.3 thread-count[quantityBrowser] ---> run test case parallel are based on the value of thread-count

==========================================================================================================
C. Project structure
==========================================================================================================
	Project root
	|___src/test/java
	|	|___Common 
	|	|	|___Constant	----------------> Defaut value for test case
	|	|
	|	|___DataObjects   ----> Data access objects
	|	|
	|	|___PageObject   ----> Page object models, contains element locators and functions to interact with AUT
	|	|
	|	|___Test   ----> All UI automated test cases
	|	|
	|	|___Wrapper   ----> Wrapper Element and Driver to manage easier
	|	|		
	|	|___Helper [ All common methods/helper in project ]
	|			|___DateHelper -----------------> Contains common methods, constants
	|			|___Listener -------------------> TestListener configuration
	|			|___Logger -------------------> Get value to print log
	|			|___Properties -----------------> Get value from properties file
	|			|___Ultilities -------------------> Provide wrapper objects/methods for Jira integration
	|			|___Reporter----------------> Customized report configuration
	|
	|___src/test/resource
		|___config -----------------> Properties to get value for Constant/Config Log4j
		|___driver -----------------> Browser web-driver that is used by Selenium
		|___suite -------------------> Contains all test suites to run the test cases
		|___grid -------------------> tools and configuration for running test case in parallel mode (we did not run this mode)

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			